<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/produits', 'ProduitController@index');
Route::post('/produit/create', 'ProduitController@store');
Route::get('/produit/{id}', 'ProduitController@show');
Route::get('/produit/delete/{id}', 'ProduitController@destroy');
Route::post('/produit/note/add', 'ProduitController@addNote');
Route::put('/produit/update', 'ProduitController@update');