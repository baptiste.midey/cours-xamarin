<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProduitsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('produits')->insert([
            "nom"=>"Call of duty - Black Ops III",
            "description"=>"Call of Duty®: Black Ops III Zombies Chronicles Edition inclut le jeu de base complet et l'extension Zombies Chronicles.

            Call of Duty: Black Ops III propose trois modes de jeu uniques : Campagne, Multijoueur et Zombies. Il offre aux fans le plus complet et le plus ambitieux opus de Call of Duty.
            
            L'extension Zombies Chronicles inclut 8 cartes Zombies classiques remasterisées de Call of Duty®: World at War, Call of Duty®: Black Ops et Call of Duty®: Black Ops II. Il s'agit des cartes complètes de la saga d'origine, entièrement remastérisées en HD et jouables dans Call of Duty®: Black Ops III. ",
            "note"=>4.7,
            "nbNote"=>825,
        ]);
        DB::table('produits')->insert([
            "nom"=>"FIFA 20",
            "description"=>"FIFA 20 est un jeu vidéo de football développé par EA Canada et EA Roumanie et édité par EA Sports1. La date de sortie du jeu, annoncée à l'E3 2019, est prévue le 27 septembre 2019 sur PC, PlayStation 4, Xbox One et Nintendo Switch. Le jeu sera également disponible le 19 septembre pendant 10 heures pour les joueurs bénéficiant de L'EA Access ou de l'Origin Access2. Une démo du jeu est disponible depuis le 10 septembre3. Trois versions différentes de cet opus sont disponibles en précommande : l'édition Standard, l'édition Champions et l'édition Ultimate4. Il s'agit du vingt-septième opus de la franchise FIFA développé par EA Sports. ",
            "note"=>3.7,
            "nbNote"=>2020,
        ]);
        DB::table('produits')->insert([
            "nom"=>"Cities: Skyline",
            "description"=>"Cities: Skylines est une version moderne de la classique simulation de ville. Le jeu introduit de nouveaux éléments de gameplay pour reproduire les sensations et difficultés inhérentes à la création et à la gestion d'une vraie ville tout en développant certains aspects établis de la construction de ville. ",
            "note"=>4.8,
            "nbNote"=>2766,
        ]);
        DB::table('produits')->insert([
            "nom"=>"Minecraft",
            "description"=>"Prépare-toi à une aventure aux possibilités illimitées lorsque tu construis, que tu casses des blocs, que tu combats des créatures ou que tu explores le paysage de Minecraft qui ne cesse de se réinventer. ",
            "note"=>4.6,
            "nbNote"=>10219,
        ]);
        DB::table('produits')->insert([
            "nom"=>"Battlefield V",
            "description"=>"Participez au plus grand conflit de l'Histoire avec Battlefield V. La licence revient aux sources et dépeint la Seconde Guerre mondiale comme jamais auparavant. Livrez des batailles multijoueur frénétiques et brutales aux côtés de votre escouade dans des modes imposants comme Grandes opérations et coopératifs comme Tir Groupé. Prenez part aux batailles méconnues de la Guerre avec les récits de guerre du mode solo. Combattez dans des environnements inédits et spectaculaires aux quatre coins du monde et découvrez le Battlefield le plus riche et le plus immersif à ce jour.",
            "note"=>4.9,
            "nbNote"=>1228,
        ]);
        DB::table('produits')->insert([
            "nom"=>"Euro Truck Simulator 2",
            "description"=>"Euro Truck Simulator 2 pour PC est le deuxième jeu de la série Truck Simulator . C'est un jeu de simulation de véhicule dans lequel le joueur traverse une version légèrement fictive de l'Europe, effectue des livraisons et des ramassages et apprend tout sur son véhicule.",
            "note"=>5.0,
            "nbNote"=>2270,
        ]);
        DB::table('produits')->insert([
            "nom"=>"Dragon Ball Fighter Z",
            "description"=>"Dragon Ball FighterZ pour PC est un jeu de combat 2.5D basé sur la série originale de mangas japonais du même nom. Malgré la majuscule Z à la fin du nom, le mot se prononce comme le mot ordinaire \"Fighter\". ",
            "note"=>4.4,
            "nbNote"=>875,
        ]);
        DB::table('produits')->insert([
            "nom"=>"Farming Simulator 19",
            "description"=>"Farming Simulator 19 pour PC marque le dixième anniversaire des jeux Farming Simulator, et la douzième édition sur toutes les plateformes de jeu. Le jeu a su conquérir un public qui lui est propre parmi les fans de la série, et cette version compte bien conquérir un nouveau public par l’ajout de nouvelles options passionnantes.",
            "note"=>4.4,
            "nbNote"=>252,
        ]);
        DB::table('produits')->insert([
            "nom"=>"ARK: Extinction Expansion Pack",
            "description"=>"Terminez votre voyage à travers les mondes de ARK dans «Extinction», où l’histoire a commencé et se termine: sur la Terre elle-même! Une planète infestée d'élément et ravagée regorgeant de créatures fantastiques à la fois organiques et technologiques, la Terre détient à la fois les secrets du passé et les clés de son salut. En tant que vétéran survivant ayant surmonté tous les obstacles précédents, votre ultime défi vous attend: pouvez-vous vaincre les gigantesques Titans errants qui dominent la planète et terminer le cycle de la ARK pour sauver l'avenir de la Terre? ",
            "note"=>3.8,
            "nbNote"=>1358,
        ]);
        DB::table('produits')->insert([
            "nom"=>"Crash Bandicoot: N. Sane Trilogy",
            "description"=>"Votre marsupial préféré, Crash Bandicoot, est de retour ! Il est amélioré, enthousiasmé et prêt à danser dans la compilation N. Sane Trilogy ! Redécouvrez Crash Bandicoot comme jamais auparavant. Tournez, sautez, tapez et recommencez pour surmonter des défis épiques et vivre des aventures extraordinaires dans les trois jeux à l'origine de tout : Crash Bandicoot, Crash Bandicoot 2: Cortex Strikes Back et Crash Bandicoot 3: Warped. Retrouvez Crash remasterisé en HD et préparez-vous, ça va WUMPER ! ",
            "note"=>4.8,
            "nbNote"=>381,
        ]);
    }
}
