<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
    @OA\Schema()
 */
class AddNote extends Model
{
    public $timestamps = false;
    /**
        id du produit à noter
        @var int
        @OA\Property()
     */
    public $Id;

    /**
        note à attribuer
        @var int
        @OA\Property()
     */
    public $Note;
}
