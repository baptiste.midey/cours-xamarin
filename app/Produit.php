<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
    @OA\Schema()
 */
class Produit extends Model
{
    public $timestamps = false;

    /**
        id du produit
        @var int
        @OA\Property()
     */
    public $Id;

    /**
        nom du produit
        @var string
        @OA\Property()
     */
    public $Nom;

    /**
        description du produit
        @var string
        @OA\Property()
     */
    public $Description;

    /**
        note du produit
        @var double
        @OA\Property()
     */
    public $Note;

    /**
        nombre de note du produit
        @var int
        @OA\Property()
     */
    public $NbNote;
}
