<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
    @OA\Schema()
 */
class NewProduit extends Model
{
    public $timestamps = false;
    /**
        nom du produit
        @var string
        @OA\Property()
     */
    public $Nom;

    /**
        description du produit
        @var string
        @OA\Property()
     */
    public $Description;
}
