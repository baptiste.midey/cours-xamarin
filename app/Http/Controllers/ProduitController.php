<?php

namespace App\Http\Controllers;

use App\Produit;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProduitController extends Controller
{
    /**
        @OA\Get(
            path="/produits",
            tags={"Produit"},
            summary="Produits",
            operationId="produits",  
            @OA\Response(
                response=200,
                description="retourne une liste de produits.",
                @OA\MediaType(
                    mediaType="application/json",
                    @OA\Schema(
                        type="array",
                        @OA\Items(ref="#/components/schemas/Produit")
                    )
                )
            ),
            @OA\Response(
                response=205,
                description="Aucun produit en stock."
            ),
            @OA\Response(
                response=404,
                description="Erreur serveur..."
            ),
        )
    */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $produits = Produit::all();
            if(isset($produits[0])){
                return response()->json($produits);
            }
            else{
                return response(null, 205);
            }
        }
        catch(Exception $e){
            return response()->json(null,404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
        @OA\Post(
            path="/produit/create",
            tags={"Produit"},
            summary="Création d'un produit",
            operationId="produitCreate",
            @OA\RequestBody(
                description="Produit à créer.",
                required=true,
                @OA\JsonContent(ref="#/components/schemas/NewProduit")
            ),
            @OA\Response(
                response=200,
                description="retourne un produit.",
                @OA\MediaType(
                    mediaType="application/json",
                    @OA\Schema(ref="#/components/schemas/Produit")
                )
            ),
            @OA\Response(
                response=404,
                description="Erreur serveur..."
            ),
            @OA\Response(
                response=403,
                description="Information manquante"
            ),
        )
    */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $validator = Validator::make($request->all(),[
                "Nom"=>"required",
                "Description"=>"required"
            ]);
            if($validator->fails()){
                return response($validator->errors(),403);
            }
            else{
                $produit = new Produit();
                $produit->nom = $request->Nom;
                $produit->description = $request->Description;
                $produit->note = 0;
                $produit->nbNote = 0;
                $produit->save();
                return response()->json($produit);
            }
        }
        catch(Exception $e){
            return response()->json(null,404);
        }
    }

    /**
        @OA\get(
            path="/produit/{id}",
            tags={"Produit"},
            summary="Recherche d'un produit",
            operationId="produitShow",
            @OA\Parameter(
                description="Id du produit.",
                in="path",
                name="id",
                required=true,
                @OA\Schema(
                    format="int32",
                    type="integer"
                )
            ),
            @OA\Response(
                response=200,
                description="retourne un produit.",
                @OA\MediaType(
                    mediaType="application/json",
                    @OA\Schema(ref="#/components/schemas/Produit")
                )
            ),
            @OA\Response(
                response=404,
                description="Erreur serveur..."
            ),
            @OA\Response(
                response=202,
                description="Produit innexistant"
            ),
            @OA\Response(
                response=403,
                description="Information manquante"
            ),
        )
    */
    /**
     * Display the specified resource.
     *
     * @param  Int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        try{
            if(isset($id)){
                $produit = Produit::find($id);
                if(isset($produit)){
                    return response()->json($produit);
                }
                else{
                    return response(null, 202);
                }
            }
            else{
                return response(null,403);
            }
        }
        catch(Exception $e){
            return response()->json(null,404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    public function edit(Produit $produit)
    {
        //
    }

    /**
        @OA\Put(
            path="/produit/update",
            tags={"Produit"},
            summary="Modification d'un produit",
            operationId="produitUpdate",
            @OA\RequestBody(
                description="Produit à modifier.",
                required=true,
                @OA\JsonContent(ref="#/components/schemas/Produit")
            ),
            @OA\Response(
                response=200,
                description="retourne un produit.",
                @OA\MediaType(
                    mediaType="application/json",
                    @OA\Schema(ref="#/components/schemas/Produit")
                )
            ),
            @OA\Response(
                response=404,
                description="Erreur serveur..."
            ),
            @OA\Response(
                response=403,
                description="Information manquante"
            ),
        )
    */
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Produit $produit)
    {
        try{
            $validator = Validator::make($request->all(),[
                "Nom"=>"required",
                "Description"=>"required",
                "Id"=>"required",
                "Note"=>"required",
                "NbNote"=>"required"
            ]);
            if($validator->fails()){
                return response($validator->errors(),403);
            }
            else{
                $produit = Produit::find($request->Id);
                $produit->nom = $request->Nom;
                $produit->description = $request->Description;
                $produit->note = $request->Note;
                $produit->nbNote = $request->NbNote;
                $produit->save();
                return response()->json($produit);
            }
        }
        catch(Exception $e){
            return response()->json(null,404);
        }
    }

    /**
        @OA\Get(
            path="/produit/delete/{id}",
            tags={"Produit"},
            summary="Suppression d'un produit",
            operationId="produitDelete",
            @OA\Parameter(
                description="Id du produit.",
                in="path",
                name="id",
                required=true,
                @OA\Schema(
                    format="int32",
                    type="integer"
                )
            ),
            @OA\Response(
                response=200,
                description="Produit supprimé."
            ),
            @OA\Response(
                response=404,
                description="Erreur serveur..."
            ),
            @OA\Response(
                response=403,
                description="Information manquante"
            ),
        )
    */
    /**
     * Remove the specified resource from storage.
     *
     * @param  Int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        try{
            if(isset($id)){
                Produit::destroy($id);
                return response("Destroyed");
            }
            else{
                return response(null,403);
            }
        }
        catch(Exception $e){
            return response()->json(null,404);
        }
    }
    /**
        @OA\Post(
            path="/produit/note/add",
            tags={"Produit"},
            summary="Ajout d'une note à un produit",
            operationId="produitAddNote",
            @OA\RequestBody(
                description="Note à ajouter au produit dont l'id est {id}.",
                required=true,
                @OA\JsonContent(ref="#/components/schemas/AddNote")
            ),
            @OA\Response(
                response=200,
                description="retourne un produit.",
                @OA\MediaType(
                    mediaType="application/json",
                    @OA\Schema(ref="#/components/schemas/Produit")
                )
            ),
            @OA\Response(
                response=404,
                description="Erreur serveur..."
            ),
            @OA\Response(
                response=403,
                description="Information manquante"
            ),
        )
    */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addNote(Request $request)
    {
        try{
            $validator = Validator::make($request->all(),[
                "Id"=>"required",
                "Note"=>"required"
            ]);
            if($validator->fails()){
                return response($validator->errors(),403);
            }
            else{
                $produit = Produit::find($request->Id);
                $produit->note = ($produit->note*$produit->nbNote+$request->Note)/($produit->nbNote+1);
                $produit->nbNote += 1;
                $produit->save();
                return response()->json(Produit::find($produit->id));
            }
        }
        catch(Exception $e){
            return response()->json(null,404);
        }
    }
}
