<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
  @OA\Info(
      description="Voici l'API qui va vous permettre de créer votre application Xamarin en utilisant des web-services.",
      version="1.0.0",
      title="Exercice Xamarin API",
 )
 */
 
/**
  @OA\SecurityScheme(
      securityScheme="bearerAuth",
          type="http",
          scheme="bearer",
          bearerFormat="JWT"
      ),
 */

/**
    @OA\Server(
        url=L5_SWAGGER_CONST_HOST,
        description="Serveur de Web-Service"
    )
 */

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
